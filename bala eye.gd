extends Area2D

const speed = 460
var velocity= Vector2()
var norantza = 1


func set_norantza(nora):
    norantza = nora
    if norantza == 1:
        $AnimatedSprite.flip_h = false
    else:
        $AnimatedSprite.flip_h = true  
        
func _physics_process(delta):
    velocity.x = speed * delta * norantza
    translate(velocity)
    velocity.y += 0.09
