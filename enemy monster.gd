extends KinematicBody2D

export (int) var speed = 200
const grabitatea = 26
export (int) var salto = -600
const BALA = preload('res://bala.tscn')
var velocity = Vector2()
var norantza = 1
var egoera='geldi'

        
func hil():
    $AnimatedSprite.play('hil')
    $Timer.start()
    
func _physics_process(delta):
    velocity.y += grabitatea
    velocity.x = speed * norantza      
    move_and_slide(velocity,Vector2(0,1))
    $AnimatedSprite.rotation_degrees = 5
    if is_on_wall():
        norantza = norantza * -1
    if norantza == 1 or norantza == -1:
        $AnimatedSprite.rotation_degrees = 5
    else:
        $AnimatedSprite.rotation_degrees = -5
        
    if norantza == 1:
        $AnimatedSprite.flip_h = true
    else:
        $AnimatedSprite.flip_h = false
    
func get_input():
    var bala = BALA.instance()
    get_parent().add_child(bala)
    bala.set_norantza(norantza)
    bala.position = $Position2D.global_position
    velocity.x = 0
    if Input.is_action_pressed('right'):
        velocity.x = speed
        $AnimatedSprite.flip_h = false
    if Input.is_action_pressed('left'):
        velocity.x = -speed
        $AnimatedSprite.flip_h = true
    if Input.is_action_pressed('up') and is_on_floor():
        velocity.y = salto
    
func _on_bala_bhody_entered(body):
    if 'enemy monster' in body.name:
        body.hil()
        queue_free()

func _on_Timer_timeout():
    $CollisionShape2D.disabled=true
    queue_free()
func _on_Timer2_timeout():
    $AnimatedSprite.rotation_degrees = -5
    
    