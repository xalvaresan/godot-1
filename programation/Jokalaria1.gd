extends KinematicBody2D

export (int) var speed = 500
const grabitatea = 26
export (int) var salto = -600
const BALA = preload('res://bala.tscn')

var velocity = Vector2()
var norantza = 1
var egoera='geldi'

func get_input():
    velocity.x = 0
    if Input.is_action_pressed('right'):
        velocity.x = speed
        $AnimatedSprite.flip_h = false
        norantza=1
    if Input.is_action_pressed('left'):
        velocity.x = -speed
        norantza=-1
        $AnimatedSprite.flip_h = true
    if Input.is_action_pressed('up') and is_on_floor():
        velocity.y = salto
    if Input.is_action_just_pressed('fire'):
        var bala = BALA.instance()
        get_parent().add_child(bala)
        bala.set_norantza(norantza)
        bala.position = $Position2D.global_position
   # if Input.is_action_pressed('fire'):
    #    bala.scale(2) 
     #   get_parent().add_child(bala)
      #  bala.position = $Position2D.global_position
        
func _physics_process(delta):
    get_input()
    velocity.y += grabitatea
    if velocity.x != 0:
        $AnimatedSprite.play('ibili')
    else:
        $AnimatedSprite.play('geldi')
    if Input.is_action_pressed('up'):
        $AnimatedSprite.play('salto')
    #if Input.is_action_pressed('fire'):
     #   $AnimatedSprite.play('shot')
        
    move_and_slide(velocity,Vector2(0, -1))
   
   # if is_on_wall():
    #    norantza=norantza * -1        $AnimatedSprite.flip_h = true     
        